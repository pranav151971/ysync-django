from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^get_url/', views.generateUrl, name="generateUrl"),
    url(r'^get_url_for_mobile/', views.generateUrlForMobile, name="generateUrlForMobile"),
    url(r'^get_all_songs_for_user/', views.getAllSongsForUser, name="getAllSongsForUser"),
    url(r'^login/', views.login, name="login"),
]
