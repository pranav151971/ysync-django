# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ySync', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('email', models.CharField(max_length=200)),
                ('device_id', models.CharField(max_length=2000)),
                ('device_type', models.CharField(max_length=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
