# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ySync', '0005_song'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='is_downloaded',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='song',
            name='is_sent',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='song',
            name='youtube_url',
            field=models.CharField(max_length=2000, null=True),
            preserve_default=True,
        ),
    ]
