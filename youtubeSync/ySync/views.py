import json
import os
import subprocess
import random
import thread


# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render

from ySync.functions import *
from ySync.models import *
from rest_framework.renderers import JSONRenderer
from rest_framework.viewsets import ModelViewSet
from rest_framework.parsers import JSONParser, FormParser

from .serializers import *


HOME = "http://139.59.16.242/"


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

def generateUrl(request):
    
    data = {}
    url = request.GET["url"]
    code = request.GET["code"]
    
    user = User.objects.filter(code = code)
    
    if(len(user) == 0):
        data["result"] = "failed"
        data["message"] = "invalid code"
        
        return HttpResponse(json.dumps(data),content_type="application/json")

    songs = Song.objects.filter(youtube_url=url)
    if len(songs) == 0:
        song = Song(
            youtube_url = url,
            user = user[0]
        )
        song.save()
        try:
           thread.start_new_thread( download_video, (song, ) )
        except:
           print "Error: unable to start thread"
    
        data["message"] = "mp3 will be sent to phone in a few seconds!"    
    else:
        if len(songs.filter(user=user[0])) > 0:
            if songs[0].is_downloaded:
                data["message"] = "song already sent" 
            else:
                data["message"] = "request in progress please wait" 
        else:
            song = Song(
                youtube_url = url,
                user = user[0],
                title = songs[0].title,
                is_downloaded = True
            )
            song.save()
            thread.start_new_thread( send_mp3, (song, ) )
            data["message"] = "mp3 will be sent to phone in a few seconds!" 
            
        
    data["result"] = "success"
    
    
    return HttpResponse(json.dumps(data),content_type="application/json")
    
def generateUrlForMobile(request):
    
    data = {}
    url = request.GET["url"]
    code = request.GET["code"]
    
    user = User.objects.filter(code = code)
    
    if(len(user) == 0):
        data["result"] = "failed"
        data["message"] = "invalid code"
        
        return HttpResponse(json.dumps(data),content_type="application/json")
    
    title = subprocess.check_output(['youtube-dl','--skip-download','--get-title',url])[:-1]
    title = clean(title)        
        
    out = subprocess.check_output(['youtube-dl','--prefer-ffmpeg', '--audio-quality' ,'9','--verbose','--embed-thumbnail','-x', '--audio-format','mp3','-o','/root/webapps/html/mp3/'+title+'.%(ext)s',url])

    song = Song(
        youtube_url = url,
        title = title,
        user = user[0],
        is_downloaded = True 
    )
    song.save()
    
    data["result"] = "success"
    data["url"] = HOME + "mp3/" + title + ".mp3" 
    data["title"] = title
    
    
    return HttpResponse(json.dumps(data),content_type="application/json")
    
def getAllSongsForUser(request):
    data = {}
    code = request.GET["code"]
    user = User.objects.filter(code = code)
    
    if(len(user) == 0):
        data["result"] = "failed"
        data["message"] = "invalid code"
        
        return HttpResponse(json.dumps(data),content_type="application/json")
    
    songs = Song.objects.filter(user = user[0])
    serializer = SongSerializer(songs, many=True)
    
    
    return(JSONResponse(serializer.data))
    
    

def login(request):
    email = request.POST["email"]
    device_id = request.POST["device_id"]
    device_type = request.POST["device_type"]
    data = {}
    devices = User.objects.filter(email = email)
    if(len(devices) == 0):
        data["code"] = ''.join(random.choice('0123456789abcdefghijklmnopqrstuvwxyz') for i in range(4))
        User(
            email = email,
            device_id = device_id,
            device_type = device_type,
            code = data["code"]
        ).save()
        data["result"] = "success"
        data["message"] = "registeration successful"
    else:
        data["result"] = "failed"
        data["message"] = "email id already in use"
        data["code"] = devices[0].code
        
    return HttpResponse(json.dumps(data),content_type="application/json")
