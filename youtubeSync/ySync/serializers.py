from .models import Song
from rest_framework import serializers




class SongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Song
        fields = ('title', 'youtube_url', 'created_date')
