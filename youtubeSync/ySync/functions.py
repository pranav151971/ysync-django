from gcm import GCM

import random
import thread
import json
import os
import subprocess

from ySync.models import *


API_KEY = "AIzaSyBlbz2GCqTAgs7k9DFv_lx5gsMq8SzCUXg"
HOME = "http://139.59.16.242/"

def clean(url):
    ex = "~!@#\/$%^&*()_+[];\',.{}|:\"?><"
    url =  ''.join([i if ord(i) < 128 else ' ' for i in url])
    for c in ex:
        url = url.replace(c,"")
    return url

def download_video(song):
    url = song.youtube_url
    
    title = subprocess.check_output(['youtube-dl','--skip-download','--get-title',url])[:-1]
    title = clean(title) 
    
    song.title = title
    
    
    out = subprocess.check_output(['youtube-dl', '--embed-thumbnail','-x', '--audio-format','mp3','-o','/root/webapps/html/mp3/'+title+'.%(ext)s',url])
    thread.start_new_thread( send_mp3, (song, ) )
    print "called send mp3"
    song.is_downloaded = True
    song.save()
    print "saved song"

def send_mp3(song):
    title = song.title
    url = song.youtube_url
    user = song.user
    gcm = GCM(API_KEY)
    gcm_data = {}
    reg_ids = [user.device_id]
    mp3_url = HOME + 'mp3/'+title +'.mp3' 
    gcm_data = { 
            'message': mp3_url,
            'notificationId': ''.join(random.choice('0123456789') for i in range(4)), 
            'title' : title
        }
    print gcm_data
    response = gcm.json_request(registration_ids=reg_ids, data=gcm_data)
    song.is_sent=True
    song.save()
    print response
    
