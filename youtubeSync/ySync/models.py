from django.db import models

class User(models.Model):
    _id = models.AutoField(primary_key=True)
    email = models.CharField(max_length=200)
    device_id = models.CharField(max_length=2000)
    device_type = models.CharField(max_length=1)
    code = models.CharField(max_length=4)
    
    def __unicode__(self):
        return self.email
        
class Song(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200,null=True)
    youtube_url = models.CharField(max_length=2000,null=True)
    user = models.ForeignKey(User, null=True)
    is_downloaded = models.BooleanField(default=False)
    is_sent = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
