from django.contrib import admin

# Register your models here.
from ySync.models import User,Song

class UserAdmin(admin.ModelAdmin):
    list_display = ('email','code')

class SongAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'youtube_url','is_downloaded','is_sent','created_date')

admin.site.register(User, UserAdmin)
admin.site.register(Song, SongAdmin)
